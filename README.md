# ZLMeiQia

[![CI Status](https://img.shields.io/travis/halvie/ZLMeiQia.svg?style=flat)](https://travis-ci.org/halvie/ZLMeiQia)
[![Version](https://img.shields.io/cocoapods/v/ZLMeiQia.svg?style=flat)](https://cocoapods.org/pods/ZLMeiQia)
[![License](https://img.shields.io/cocoapods/l/ZLMeiQia.svg?style=flat)](https://cocoapods.org/pods/ZLMeiQia)
[![Platform](https://img.shields.io/cocoapods/p/ZLMeiQia.svg?style=flat)](https://cocoapods.org/pods/ZLMeiQia)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

ZLMeiQia is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'ZLMeiQia'
```

## Author

halvie, 3273948281@qq.com

## License

ZLMeiQia is available under the MIT license. See the LICENSE file for more info.
