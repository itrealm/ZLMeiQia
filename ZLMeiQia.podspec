#
# Be sure to run `pod lib lint ZLMeiQia.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'ZLMeiQia'
  s.version          = '0.1.4'
  s.summary          = 'A short description of ZLMeiQia.'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
TODO: Add long description of the pod here.
                       DESC

  s.homepage         = 'https://codeup.aliyun.com/zeekrlife/zeekrlife-ios/zeekrlife-ios-module'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'halvie' => '3273948281@qq.com' }
  s.source           = { :git => 'https://gitlab.com/itrealm/ZLMeiQia.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '9.0'
  s.requires_arc = true
  s.pod_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }
  s.user_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }

  # s.source_files = 'ZLMeiQia/Classes/**/*'

  s.subspec 'MeiqiaSDK' do |ss|
    ss.frameworks =  'AVFoundation', 'CoreTelephony', 'SystemConfiguration', 'MobileCoreServices'
    ss.vendored_frameworks = 'ZLMeiQia/MeiqiaSDK.framework'
    ss.libraries  =  'sqlite3', 'icucore', 'stdc++'
    ss.xcconfig = { "FRAMEWORK_SEARCH_PATHS" => "${PODS_ROOT}/ZLMeiQia/ZLMeiQia"}
  end
  s.subspec 'MQChatViewController' do |ss|
    ss.dependency 'ZLMeiQia/MeiqiaSDK'
    # avoid compile error when using 'use frameworks!',because this header is c++, but in unbrellar header don't know how to compile, there's no '.mm' file in the context.
    ss.private_header_files = 'ZLMeiQia/MQChatViewController/Vendors/VoiceConvert/amrwapper/wav.h'
    ss.source_files = 'ZLMeiQia/MeiqiaSDKViewInterface/*.{h,m}', 'ZLMeiQia/MQChatViewController/**/*.{h,m,mm,cpp}', 'ZLMeiQia/MQMessageForm/**/*.{h,m}'
#    ss.vendored_libraries = 'ZLMeiQia/MQChatViewController/Vendors/MLAudioRecorder/amr_en_de/lib/libopencore-amrnb.a', 'ZLMeiQia/MQChatViewController/Vendors/MLAudioRecorder/amr_en_de/lib/libopencore-amrwb.a'
    #ss.preserve_path = '**/libopencore-amrnb.a', '**/libopencore-amrwb.a'
    ss.xcconfig = { "LIBRARY_SEARCH_PATHS" => "\"$(PODS_ROOT)/ZLMeiQia/ZLMeiQia\"" }
    ss.resources = 'ZLMeiQia/MQChatViewController/Assets/MQChatViewAsset.bundle'
  end
  

end


#修改podspec版本号
#   cd到Example 终端命令pod install
#   回到根目录 cd ../
#   git add .
#   git commit -m "更新内容"
#   git tag -a 0.1.0 -m “版本VersonTag”
#   git push origin --tags
#   git push -u origin master

# ZLMeiQia----本地校验
#依赖用户中心时需跳过引入校验
#pod lib lint --use-libraries --allow-warnings --skip-import-validation --verbose --sources='https://gitlab.com/itrealm/ZLMeiQia.git'



#pod repo push ZLMeiQiaRepo ZLMeiQia.podspec --allow-warnings --use-libraries
