//
//  main.m
//  ZLMeiQia
//
//  Created by halvie on 12/06/2021.
//  Copyright (c) 2021 halvie. All rights reserved.
//

@import UIKit;
#import "MQAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([MQAppDelegate class]));
    }
}
