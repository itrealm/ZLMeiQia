//
//  MQAppDelegate.h
//  ZLMeiQia
//
//  Created by halvie on 12/06/2021.
//  Copyright (c) 2021 halvie. All rights reserved.
//

@import UIKit;

@interface MQAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
